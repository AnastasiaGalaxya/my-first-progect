#6

def sum_dict(a, b):
    return {**a, **b}

print(sum_dict({1: 3, 4: 8}, {2: 5, 3: 6}))
