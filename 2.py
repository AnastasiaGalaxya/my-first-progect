#2
def my_decor(function):
    def my_time_check():       
        import time
        start = time.time()
        function()
        finish = time.time()
        print(finish - start)
    return  my_time_check

@my_decor
def ddnednd():
    print(2 ** 25)

ddnednd()



