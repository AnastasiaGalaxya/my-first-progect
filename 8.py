#8

def reverse_my_list(lst):
    lst[0], lst[len(lst)-1] = lst[len(lst)-1], lst[0]
    return lst

print(reverse_my_list(['a', 8, 9, 32, 24]))
