class Bird:
    pass

Polly = Bird()
print(Polly)

Kesha = Bird()
print(Kesha)

a = Kesha
b = Polly
print(a == b)

class Bird:
    kingdom = 'animals'
    def __init__(self, species, age):
        self.species = species
        self.age = age

kedra = Bird('Black Barracks', 3)
milli = Bird('White Goose', 1)

print(kedra.species)
print(kedra.age)


print(milli.species)
print(milli.age)

print(kedra.kingdom)

kedra.age = 5
print(kedra.age)

milli.kingdom = 'plants'
print(milli.kingdom)

class Bird2:
    kingdom = 'plants'

    def __init__(self, species, age, gender):
        self.species = species
        self.age = age
        self.gender = gender

#    def specification(self):
#        return f"{self.species} is {self.age} years old"
    
    def __str__(self):
        return f"{self.species} is {self.age} years old"

    def speak(self, song):
        return f"{self.species} screaming: {song}"

#barry = Bird2("Black Barracks", 2)
#print(barry.specification)

#print(barry.speak("KyKy"))

#print(barry.speak("ChiricChiric"))
#print(barry)

dasy = Bird2("Stercorarius skua ", 2, 'w')
sam = Bird2("Stercorarius skua ", 1, 'm')
juletta = Bird2('White Goose', 1, 'w')
rosa = Bird2('White Goose', 7, 'w')

print(dasy.speak("Chirik"), '\n', juletta.speak("Gagaga"), '\n', rosa.speak("Gagaga"))
class Goose(Bird2):
    def speak(self, song = 'GaGaGa'):
        return super().speak(song)

class Stercorarius(Bird2):
        pass
dasy = Stercorarius("Stercorarius skua ", 2, 'w')
sam = Stercorarius("Stercorarius skua ", 2, 'w')
juletta = Goose('White Goose', 1, 'w')
rosa = Goose('White Goose', 1, 'w')

print(dasy.kingdom)
print(sam.species)
print(juletta)
print(rosa.speak('GaGaGa'))
print(type(dasy))
print(isinstance(rosa, Stercorarius))
print(isinstance(dasy, Goose))
print(rosa.speak())
print(rosa.speak('Gaaaaa'))

garry = Stercorarius("Stercorarius skua ", 5, 'm')
print(garry.speak('Chirik'))
print(rosa.speak())
